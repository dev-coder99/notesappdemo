import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Note } from '../../../../core/models/note';
import { NoteService } from '../../../../core/services/note.service';
import _ from "lodash";

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {

  // Form properties
  public noteForm: FormGroup;
  public submitted: boolean = false;
  public notes: Note[];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private noteService: NoteService
  ) {
    this.setNoteForm(new Note());
   }

  ngOnInit() {
    this.getNotes();
  }

  // Get Notes
  getNotes() {
    this.noteService.get().subscribe(
      (notes: Note[]) => {
        this.notes = notes;
      }
    );
  }

  // set note form 
  setNoteForm(note: Note) {
    this.noteForm = this.formBuilder.group({
      frmId: new FormControl(note.NoteId),
      frmTitle: new FormControl(note.Title, [
        Validators.required,
      ]),
      frmBody: new FormControl(note.Body, [Validators.maxLength(100)])
    });
  }

  // on Click of Save button
  onSave() {
    this.submitted = true;
    if (this.noteForm.invalid) {
      return;
    }

    let noteModel = new Note();
    noteModel.NoteId = this.noteForm.value.frmId || 0;
    noteModel.Title = this.noteForm.value.frmTitle;
    noteModel.Body = this.noteForm.value.frmBody;
    
    this.noteService.addEdit(noteModel).subscribe(
      (notes: any) => {
        this.notes = notes.length ? notes : this.notes;
        this.noteForm.reset();
      },
      error => {
        alert('something went wrong!');        
      }, 
      () => {
        this.submitted = false;        
      }
    )
  }

  // On Click of Add note button
  onAddNote() {
    this.noteForm.reset();
  }

  // On Edit note 
  onEditNote(note: Note) {
    this.setNoteForm(note);
  }

  // Remove Note 
  onRemoveNote (noteId :string) {

    let notes = JSON.parse(localStorage.getItem('notesData'));
    _.remove(notes, function(note) { return note.NoteId == noteId; });
    localStorage.setItem('notesData', JSON.stringify(notes));
    this.getNotes();
  }

  noteFormControls() {
    return this.noteForm.controls;
  }

}
