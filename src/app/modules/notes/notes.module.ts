import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteComponent } from './pages/note/note.component';
import { NotesRoutingModule } from './notes-routing.module';
import { SharedModule } from '../../shared';

@NgModule({
  declarations: [NoteComponent],
  imports: [
    CommonModule,
    NotesRoutingModule,
    SharedModule
  ]
})
export class NotesModule { }
