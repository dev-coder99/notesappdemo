import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NoteComponent } from './pages/note/note.component';
import { AuthGuard } from '../../core/services/auth-guard.service';

const routes: Routes = [
  {
    path: "",
    component: NoteComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotesRoutingModule {}
