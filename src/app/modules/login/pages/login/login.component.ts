import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Login } from '../../../../core/models/login';
import { LoginService } from '../../../../core/services/login.service';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Form properties
  public loginForm: FormGroup;
  public submitted: boolean = false;
  public emailPattern: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) {
    window.localStorage.setItem(environment.validUser, "false");
    this.setLoginForm(new Login());
   }

  ngOnInit() {
    this.loginForm.reset();
  }

  // set login form by passing model with value
  setLoginForm(login: Login) {
    this.loginForm = this.formBuilder.group({
      frmEmail: new FormControl(login.email, [
        Validators.required,
        Validators.pattern(this.emailPattern)
      ]),
      frmPassword: new FormControl(login.password, Validators.required)
    });
  }

  // To get Form control in view
  loginFormControl() {
    return this.loginForm.controls;
  }

  // On Login click 
  onLogin() {
    this.submitted = true;

    if (!this.loginForm.valid) {
      return;
    }
    
    let loginModel = new Login();
    loginModel.email = this.loginForm.value.frmEmail.toString().trim();
    loginModel.password = this.loginForm.value.frmPassword.toString().trim();

    this.loginService.validateUser(loginModel).subscribe(
      (loginState: any) => {
        if (loginState) 
        {
          alert('Login successfully');
          window.localStorage.setItem(environment.validUser, "true");
          this.router.navigateByUrl('/notes');
        } 
        else
        {
          alert('invalid credentials');
          this.loginForm.reset();
        }
      },
      error => {
        alert('something went wrong!');        
      }, 
      () => {
        this.submitted = false;        
      }
    );
  }

}
