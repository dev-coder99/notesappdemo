import { Injectable } from '@angular/core';
import { Login } from '../models/login';
import { of, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  validateUser(loginModel: Login): Observable<boolean> {
    return of(
      JSON.parse(window.localStorage.getItem(environment.credentials)).email == loginModel.email && 
      JSON.parse(window.localStorage.getItem(environment.credentials)).password == loginModel.password
    );
  }

  isUserLoggedIn(): boolean {
    debugger
    return window.localStorage.getItem(environment.validUser) == 'true';
  }
}
