import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Note } from '../models/note';
import _ from "lodash";
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor() { }

  get() : Observable<Note[]> {
    return of (localStorage.getItem(environment.notesData) ? JSON.parse(localStorage.getItem(environment.notesData)) : []);
  }

  addEdit(note: Note): Observable<Note[]> {
    
    // ADD Note
    if (note.NoteId == 0) 
    {
      if (window.localStorage.getItem(environment.notesData) && JSON.parse(localStorage.getItem(environment.notesData)).length) 
      {
        let notes = JSON.parse(localStorage.getItem(environment.notesData));
        let lastAddedNote = _.last(notes);
        note.NoteId = lastAddedNote.NoteId + 1;
        notes.push(note);  
        localStorage.setItem(environment.notesData, JSON.stringify(notes));
      } 
      else 
      {
        note.NoteId = 1;
        let notes = [note];
        localStorage.setItem(environment.notesData, JSON.stringify(notes));
      }
      return of (JSON.parse(localStorage.getItem(environment.notesData)));
    }
    // EDIT Note
    else 
    {
      let notes = JSON.parse(localStorage.getItem(environment.notesData));
      if (notes.length) 
      {
        notes.forEach(_note => {
          if (_note.NoteId == note.NoteId) {
            _note.Title = note.Title;
            _note.Body = note.Body;
          }
        });
      }
      localStorage.setItem(environment.notesData, JSON.stringify(notes));

      return of (notes);
    }
  }
}
