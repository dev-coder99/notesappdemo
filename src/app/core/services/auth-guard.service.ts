import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './login.service';


@Injectable({
  providedIn: "root"
})
export class AuthGuard {
  constructor(
    private router: Router,
    private loginSerice: LoginService,
  ) {}

  public redirectTo: any;

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    this.redirectTo = route.data["redirectTo"] as Array<string>;

    if (this.loginSerice.isUserLoggedIn()) {
      return true;
    } else {
      this.router.navigateByUrl('/');
      return false;
    }
  }
}
