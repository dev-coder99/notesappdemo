import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../core/services/login.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-content-layout',
  templateUrl: './content-layout.component.html',
  styleUrls: ['./content-layout.component.scss']
})
export class ContentLayoutComponent implements OnInit {

  constructor(
    private router: Router,
    private loginSerice: LoginService,
  ) { }

  ngOnInit() {
    this.setUserData();
  }

  setUserData() {
    window.localStorage.setItem(environment.credentials, "{\"email\": \"test@gmail.com\", \"password\": \"Test123.\"}");
  }

  isLoggedIn() {
    return this.loginSerice.isUserLoggedIn();
  }

  logout() {
    this.router.navigateByUrl('/');
  }

}
