

How To run the project :
1. First download or clone project
2. Run "npm install" command to install all dependencies
3. After that run "ng serve" command to run the application.

-- GENERAL GUIDE --
=> Application having total two pages (login, note screen)
=> Authentication is working for application, means you can't access notes screen without login.
=> You can see Login page as landing screen.
=> Default UserName is "test@gmail.com" and password is "Test123."
=> After successfully loggedin, you can see the notes screen.

-- NOTE SCREEN -- 
=> You can enter "Title" and "Body" and click on save button to save notes.
=> Saved notes will come into the left side panel (card)
=> On click of specific card title you can edit that note details
=> On click of cross icon, you can remove the note.
=> On click of "Add Notes" button the form is reset and you can enter new note.


# NoteApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
